import "./App.css";
import "antd/dist/antd.css";
import React, { useState, useEffect } from "react";
import { Card, Avatar } from "antd";
// COMPONENT <Meta />
const { Meta } = Card;

/* VARIáVEIS */
const vars = {
  userNameIcon: <span className="icon fas fa-search">{" search"}</span>,
  newUserNameIcon: <span className="icon fas fa-search">{" new user"}</span>,
  messageError: <span className="message-error">{"INVALID USERNAME"}</span>,
};
const { userNameIcon, newUserNameIcon, messageError } = vars;

// <APP />
const App = () => {
  //STATES
  const [user, setUser] = useState({});
  const [active, isActive] = useState(false);
  const [userName, setUserName] = useState("");

  //FETCH API and SET STATES
  const handleToggle = () => {
    fetch(`https://api.github.com/users/${userName}`)
      .then((res) => res.json())
      .then((gitUser) => setUser({ ...gitUser }), isActive(!active));
  };

  //SET userName from INPUT
  const getUserName = (e) => {
    setUserName(e.target.value);
  };

  //didMount
  useEffect(() => {
    handleToggle();
  }, []);

  return (
    <div className="App">
      <div>
        <div className="btn-toogle">
          {active && (
            <input
              onChange={getUserName}
              value={userName}
              placeholder="username"
            />
          )}
          <button onClick={handleToggle}>
            {active ? userNameIcon : newUserNameIcon}
          </button>
        </div>
        {!active && (
          <Card style={{ width: 300 }} cover={<img src={user.avatar_url} />}>
            <Meta
              avatar={<Avatar src={user.avatar_url} />}
              title={user.name || messageError}
              description={user.html_url}
            />
            <hr />
            <p>{user.bio}</p>
          </Card>
        )}
      </div>
    </div>
  );
};

export default App;
